# JWTSpring

Source code for examples of JWT Usage with Springboot used for Transfer of Knowledge internal [DGT](https://www.pajak.go.id).

This repository is for educational purpose.

## Canonical source

The canonical source of this repository is [hosted on GitLab.com](https://gitlab.com/dadangnh/jwt-spring).

# Springboot installation and help

Please check [this file](HELP.md).

# License

This code is published under [MIT License](LICENSE).
