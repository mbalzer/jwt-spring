package com.dadangnh.jwtspring;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.Test;

public class JWTRSATest {
    @Test
    public void testJWTWithRsa() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
        keyGenerator.initialize(4096);

        KeyPair kp = keyGenerator.genKeyPair();
        PublicKey publicKey = kp.getPublic();
        PrivateKey privateKey = kp.getPrivate();

        String encodedPublicKey = Base64.getEncoder().encodeToString(publicKey.getEncoded());
        String token = generateJwtToken(privateKey);
        System.out.println("==================== Data Information ====================");
        System.out.println("Public Key:");
        System.out.println(convertToPublicKey(encodedPublicKey));
        System.out.println("TOKEN:");
        System.out.println(token);
        System.out.println("================== End Data Information ==================");
        System.out.println("");
        printOutput(token, publicKey);
    }

    @SuppressWarnings("deprecation")
    public String generateJwtToken(PrivateKey privateKey) {
        String token = Jwts.builder().setSubject("dadang")
                .setExpiration(new Date(2023, Calendar.FEBRUARY, 1))
                .setIssuer("hello@dadannh.com")
                .claim("ROLES", new String[] { "ROLE_ADMIN", "ROLE_DEVELOPER" })
                // RS256 with privateKey
                .signWith(SignatureAlgorithm.RS256, privateKey).compact();
        return token;
    }

    public void printOutput(String token, PublicKey publicKey) {
        Jws <Claims> parseClaimsJws = Jwts
                .parserBuilder()
                .setSigningKey(publicKey)
                .build()
                .parseClaimsJws(token);

        System.out.println("===================== Test IAM Token =====================");
        System.out.println("Header     : " + parseClaimsJws.getHeader());
        System.out.println("Body       : " + parseClaimsJws.getBody());
        System.out.println("Signature  : " + parseClaimsJws.getSignature());
        System.out.println("=================== End Test IAM Token ===================");
    }

    // Add BEGIN and END comments
    private String convertToPublicKey(String key){
        StringBuilder result = new StringBuilder();
        result.append("-----BEGIN PUBLIC KEY-----\n");
        result.append(key);
        result.append("\n-----END PUBLIC KEY-----");
        return result.toString();
    }
}
