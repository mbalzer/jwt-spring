package com.dadangnh.jwtspring;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Test;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class JWTIAMTest {
    private String publicKeyString = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA1yV6s3t5nSHdZWd01Gl7M3/gn+XrUYXcvUqwDGwoCvjEKvTz6a3x3Y8NZn67MjGp8sNkEsK1agiI3HUsdXsbWt+S1f/K7GROpHkvKyVRhWamDEtM6ocDYufl0NEZ918UjXZUrzq1oS0T0H+fSuwaD595iRvE6NP8igne6z2+4zfLaVvZLEk2V2EMyxLK4vSzyvnfJOhKojlKS34eainC05rugyccoxletEQPYC7KRiLX0xg+AIpvSl/pNoQWc2LNreiCZvHldfJNaSoda42quQ0i8iMaPg5ZcHyGYU8Yi0TpLDNifyOijrpUkSQD/BnXpzhkn6slBLwbTGJV7/MGyhXA0ZQ5pBj90N8BvTwkl6XgXe7/NARLmeDNKof7mrI1X5gE6ahuXASihp2rUNfbXcxUJpuLr9WrtmL72XQU62v2acGKGErAdWPLUaRNnJX1gyBD5nd3Iv9MB2LjyAsjXLmVfQCj8zkRDjBefO2m8JXanZWQ1BYt95k4lMynmABjV/YP1YlUHVbMxUYScOtBm/XnGhPglsG71D7xZQfFDBWaVsdXlAeVggNn5NCE706VjvPm0av/arUbO8YJ1wbYiFOaY1ig9lZDs/GgJ/6SM8VQUz3skfC0RNbTJFIfhb0ITiAqdDEnDWBq1oMgEwIBvYJ/5WG/ZUbvfwf64kgjvh0CAwEAAQ==";

    @Test
    public void testJWTIAM() throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Token generated yesterday
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2NDEyMzIxODUsImV4cCI6MTY0MTIzNTc4NSwicm9sZXMiOlsiUk9MRV9VU0VSIiwiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6ImFkbWluIiwiaWQiOiJjMmFkNDc5OS00Njc0LTRmYzEtOTcwYS05ZjgyNDc3NmFmM2EiLCJleHBpcmVkIjoxNjQxMjM1Nzg1LCJwZWdhd2FpIjpudWxsfQ.ggL5VePAPVVe_02QY4cO9rKOVSWjpO9GIRMcBa3TglyGH8mA1C44RDBOBR7X7w59X2TfDTyjqtVrWLFNo9IemeHhBOavfuJVLOqx39sKjcIwLfkUHAej4k8tkhU3CvdO6lW6VTsJuVnPsqCIU_mxLfT1_89X7DQWBEvYY2LZ2_neklnvo9h81HlXwMZU28b0vXxg4JAYzkA_cEkIW8P47Tw6mjqevIdoxe1o6Oh_EjGe4NmZdg8XrZrWbuMjYl1nRH7lrVqYO7855tFBqjq849YLdfc6S7vNzVndkZgC0gTIl3osTUylVexzNhIuBzwD2ddwUkylpixmY9ptPiURX9EDJMIaJhk0z4Mvge6ZGwIr6VFKJGtDZG_8geqlu3myOauMsHycVg1HD51Qswd5Uq1l70WXx7rPPQ8A_ZoCShK7WbZTHr9v-p22Nw0KF_SH9STtbugRgsu61VKpTdAb6ArHLDA0-NBP87he_3rePbIr_B4Zjy4YXw81UPl1Rd5edqsVUZ3PSlxWxp7BhoT1c1wO69vTTaj-26RYSo4JCdPAA1zJVh4CdRCmsS9_NjrHi2oZv2KNGkIruRGKor1ne-wfJDFEJqzVbKyplWFJUr9N6Lx2RcoxAbbctn4IZqw-DwgIh4Nc-gts6eDQOpjcBFO7wuTaD608qjpxPTGPgdU";

        // Valid token test
//        String token = "";

        this.processToken(token);
    }

    private void processToken(String token) throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Base64 decoding to byte array
        byte[] publicKeyByteServer = Base64.getDecoder().decode(this.publicKeyString);

        // generate the publicKey
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = (PublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(publicKeyByteServer));
        printOutput(token, publicKey);
    }

    private void printOutput(String token, PublicKey publicKey) {
        Jws <Claims> parseClaimsJws = Jwts
                .parserBuilder()
                .setSigningKey(publicKey)
                .build()
                .parseClaimsJws(token);
        System.out.println("==================== Data Information ====================");
        System.out.println("PUBLIC KEY:");
        System.out.println(publicKey);
        System.out.println("TOKEN:");
        System.out.println(token);
        System.out.println("================== End Data Information ==================");
        System.out.println("");
        System.out.println("===================== Test IAM Token =====================");
        System.out.println("Header     : " + parseClaimsJws.getHeader());
        System.out.println("Body       : " + parseClaimsJws.getBody());
        System.out.println("Signature  : " + parseClaimsJws.getSignature());
        System.out.println("=================== End Test IAM Token ===================");
    }
}
